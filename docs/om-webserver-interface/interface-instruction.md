#### Get specify iterm from community ####

**api：** http://192.168.0.1:8888/query/{item}?community={communty_name}

**item:**

|item pamameters|description|community scope|specific instruction|request example| normal result body| exception result body|  
|  -----------  | ----------| ----  | -------------------| ----  | ----| ----  |
| contributors  | 贡献者 |openEuler, openGauss, openLookeng|GET request|curl -X GET http://192.168.0.1:8888/query/contributors?community=openEuler |```{"code":200,"data":{"contributors":8353},"msg":"OK"}```|----|
| avgduration | 每个项目(project),每个架构类型(hostarch),每个包(package)的平均构建时间 |openEuler|GET request|curl -X GET http://192.168.0.1:8888/query/avgduration?community=openEuler |----|----|
| sigs  | sig组 |openEuler|GET request|curl -X GET http://192.168.0.1:8888/query/sigs?community=openEuler |```{"code":200,"data":{"sigs":95},"msg":"OK"}```|----|
| users  | 社区用户 |openEuler, openGauss, openLookeng|GET request|curl -X GET http://192.168.0.1:8888/query/users?community=openEuler |```{"code":200,"data":{"users":404597},"msg":"OK"}```|----|
| noticeusers  | 参与活动人次 |目前还没有社区开通此接口|GET request|curl -X GET http://192.168.0.1:8888/query/noticeusers?community=openEuler |```{"code":200,"data":{"noticeusers":897},"msg":"OK"}```|{"code":404,"data":{"noticeusers":0},"msg":"not Found!"}|
| modulenums  | 仓库软件 |openEuler|GET request|curl -X GET http://192.168.0.1:8888/query/modulenums?community=openEuler |```{"code":200,"data":{"modulenums":8778},"msg":"OK"}```|---|
| businessosv  | 本社区商业版本个数|openEuler,openGauss,openLookeng|GET request|curl -X GET http://192.168.0.1:8888/query/businessosv?community=openEuler |```{"code":200,"data":{"businessOsv":11},"msg":"OK"}```|----|
| communitymembers  | 用户总数|openEuler, openGauss, openLookeng|GET request|curl -X GET http://192.168.0.1:8888/query/communitymembers?community=openEuler |```{"code":200,"data":{"communitymembers":8453},"msg":"OK"}```|----|
| all  | 给定community所有统计总量数据，包括：总下载数（downloads），总贡献者数(contributors)，总用户数(users)，总关注用户数（noticeusers）,总sig组数，总仓库软件数， 总商业版本数， 总commnunity成员数|openEuler|GET request|curl -X GET http://192.168.0.1:8888/query/all?community=openEuler |```{"code":200,"data":{"downloads":0,"contributors":8352,"users":404597,"noticeusers":0,"sigs":95,"modulenums":8778,"businessosv":11,"communitymembers":8352},"msg":"OK"}```|----|
| stars  | 社区中star的总数 |openEuler，openGauss, openLookeng, mindSpore | GET request|curl -X GET http://192.168.0.1:8888/query/all?community=openEuler |```{"code":200,"data":{"stars":8353},"msg":"OK"}```|----|
| issues  | 社区中issues总数 |openEuler，openGauss, openLookeng, mindSpore | GET request|curl -X GET http://192.168.0.1:8888/query/isssues?community=openEuler |```{"code":200,"data":{"issues":8353},"msg":"OK"}```|----|
| prs  | 社区中pr总数 |openEuler，openGauss, openLookeng, mindSpore | GET request|curl -X GET http://192.168.0.1:8888/query/prs?community=openEuler |```{"code":200,"data":{"prs":8353},"msg":"OK"}```|----|
| downloads  | 社区中download总数 |openGauss,mindSpore | GET request|curl -X GET http://192.168.0.1:8888/query/downloads?community=openGauss |```{"code":200,"data":{"downloads":8353},"msg":"OK"}```|----|
| starFork  | 社区中stars与forks总数 |openharmony, openeuler, mindspore, opengauss, openlookeng, kubeedge,volcano,soda,karmada,carbondata,liteos, servicecombo, openarkcompiler, ng-devui, vue-devui, smarts, cv-backbones, bolt, addernet, pretained-language-model, vega, trustworthyai, bht-arima, bgcn, streamdm, streamdm-cpp, allproject|GET request;  1.community支持多个组织, 以","分割; 2. 不区分大小写; 3. 当community = allproject时, 返回所有项目的数据|curl -X GET http://192.168.0.1:8888/query/starFork?community=openEuler,xxx |```{"msg":"OK","code":200,"data":[{"forks":28640,"stars":4472,"community":"openeuler"}]}```|----|
| sigs  | sig组 |openEuler|GET request|curl -X GET http://192.168.0.1:8888/query/all?community=openEuler |----|----|


# 社区数据获取接口 #
## 请求方式 ##
* GET

## 参数格式 ##
* community scope: openEuler,openGauss
* timeRange scope: lastonemonth,lastoneyear,lasthalfyear
* date: YYYY-MM-DD
* contributeType scope: pr,issue,comment

## 请求接口和返回结果 ##

* 获取社区的所有sig名称
```
http://192.168.0.1:8888/query/sig/name?community=openeuler
{"msg":"success","code":200,"data":{"openeuler":["sig-A","sig-B",...]}}
```

* 获取指定sig下的所有仓库
```
http://192.168.0.1:8888/query/sig/repo?community=openeuler&sig=sig-A&timeRange=lastonemonth
{"msg":"success","code":200,"data":{"sig-A":["XXX","YYY",...]}}
```

* 获取指定sig的活跃度及排名
```
http://192.168.0.1:8888/query/sig/sigscores?community=openeuler&sig=sig-A&timeRange=lastoneyear&date=2022-03-01
{"msg":"success","code":200,"data":{"sig":"sig-A","score":0.68714060370791064,"rank":18}}
```

* 获取指定sig的活跃度指标
```
http://192.168.0.1:8888/query/sig/sigdetails?community=openeuler&sig=sig-A&timeRange=lastoneyear&date=2022-03-01
{"msg":"success","code":200,"data":{"sig-A":[15,13,4,6,6,31,2,0,0,95,1,4,3],"metrics":["D0","D1","D2","Company","PR_Merged","PR_Review","Issue_update","Issue_closed","Issue_comment","Contribute","Meeting","Attendee","Maintainer"]}}
```

* 获取所有sig的活跃度及排名
```
http://192.168.0.1:8888/query/allsigscores?community=openeuler&timeRange=lastoneyear&date=2022-03-01
{"msg":"success","code":200,"data":{"sig-A":{"sig":"sig-A","score":0.5560699962197033,"rank":40},"sig-B":{"sig":"sig-B","score":0.351184115607382,"rank":73},...}}}
```

* 获取指定sig下用户的个人贡献
```
http://192.168.0.1:8888/query/sig/usercontribute?community=openeuler&timeRange=lastoneyear&sig=sig-A&contributeType=issue
{"msg":"success","code":200,"data":[{"gitee_id":"xxx","contribute":10,"usertype":"maintainers"},{"gitee_id":"yyy","contribute":1,"usertype":"committers"}]}
```

* 获取社区的企业名称
```
http://192.168.0.1:8888/query/company/name?community=openeuler
{"msg":"success","code":200,"data":{"openeuler":["company-A","company-B","company-C"]}}
```

* 获取指定企业的员工的个人贡献
```
http://192.168.0.1:8888/query/company/usercontribute?community=openeuler&timeRange=lastoneyear&contributeType=pr&company=company-A
{"msg":"success","code":200,"data":[{"gitee_id":"xxx","contribute":98,"usertype":"contributor"},{"gitee_id":"yyy","contribute":12,"usertype":"maintainers"},{"gitee_id":"zzz","contribute":19,"usertype":"maintainers","is_TC_owner":1}]}
```

* 获取指定企业的员工在所有sig下的各项贡献指标
```
http://192.168.0.1:8888/query/company/sigdetails?community=openeuler&timeRange=lastoneyear&company=company-A
{"msg":"success","code":200,"data":{"company-A":{"sig-A":[1,0,0,1,0,0,0,0,0,2,0,0,0],"sig-B":[8,5,1,1,1,0,0,0,0,364,0,0,0],"metrics":["D0","D1","D2","Company","PR_Merged","PR_Review","Issue_update","Issue_Closed","Issue_Comment","Contribute","Meeting","Attebdee","Maintainer"]}}
```



# 埋点数据采集接口 #
#### 请求接口
```
http://192.168.0.1:8888/query/track?data=xxx&ext=xxx
```

## 请求方式 ##
* GET

## 返回结果 ##
```
{"code":200,"track_id":"xxxxx","msg":"collect over"}
```

<br/>

#### POST specify iterm from community ####

# 蓝区人员贡献数据查询 #
#### 请求接口
```
http://192.168.0.1:8888/query/blueZone/contributes
```

## 请求方式 ##
* POST（仅有POST请求）

## 请求body ##
```
{
  "token": "xxxxx",  //必须
  "startTime": "YYYY-MM-DD",  //可选，YYYY-MM-DD
  "endTime": "YYYY-MM-DD",  //可选，YYYY-MM-DD
  "gitee_id": ["zhangsan", "lisi"],    //可选，列表
  "github_id": ["wangwu", "zhaoliu"]  //可选，列表
}
```
<font color="red">说明：时间范围为 startTime 00:00:00 至 endTime 23:59:59，既包含endTime当天的数据</font>

## 返回结果 ##
<font color="red">说明：一次返回的数据最多1W条，既prs、comments、commit、issue列表数据总条数最多 1W条，若超过1W可以通过时间段多次请求</font>


示例：
```
{
    "msg": "OK",
    "code": 200,
    "data": 
  {
        "prs":   //pull_request
    [
            {
                "created_at": "2020-12-23T11:24:53+08:00"
                "pr_title": "禁止用户向内部模式添加表", 
                "pr_state": "merged",
                "repo": "https://gitee.com/opengauss/openGauss-server",
                "is_pr": 1,
                "pull_id": 3267209,
                "pr_body": "",
                "name": "xxx",
                "org": "openGauss",
                "gitee_id": "xxx",
                "github_id": "",
                "emails": "xxx@xxx",
                "id": "ada522fcc2c839d87d487fceb6dbbacc"
            }
        ],
        "comments":   //pr和issue评论
    [
            {
                "created_at": "2020-12-30T21:56:13+08:00",
                "id": "35b2066e66aad57b4bbb52497c6326aa",
                "repo": "https://gitee.com/opengauss/openGauss-server",
                "is_comment": 1,
                "comment_body": "",
                "name": "xxx",
                "org": "openGauss",
                "gitee_id": "xxx",
                "github_id": "",
                "emails": "xxx@xxx"
            }
        ],
        "commits":     //commit信息
    [
            {
                "created_at": "2020-11-24T20:51:13+0800",
                "commit_id": "cdd577685758b9a6719f70a33bd2b7b11b9ce9f3",
                "repo": "https://gitee.com/opengauss/openGauss-server",
                "line_add": 23,
                "line_remove": 2,
                "is_commit": 1,
                "name": "xxx",
                "org": "openGauss",
                "gitee_id": "xxx",
                "github_id": "",
                "emails": "xxx@xxx",
                "id": "d4fbdce2354e5c765dbcac0037ca1799"
            }
        ],
        "issues":      //issue
    [
            {
                "created_at": "2021-02-22T12:07:21+08:00",
                "issue_title": "支持静态文件修改工具",
                "issue_state": "open",
                "repo": "https://gitee.com/opengauss/openGauss-OM",
                "is_issue": 1,
                "issue_id": 5435720,
                "issue_body": "",
                "name": "xxx",
                "org": "openGauss",
                "gitee_id": "xxx",
                "github_id": "",
                "emails": "xxx@xxx",
                "id": "4536af34be166ea5a05b437a63d8ffb2"
            }
        ]
    }
}
```


# 蓝区人员用户添加 #
#### 请求接口
```
http://192.168.0.1:8888/query/blueZone/users
```

## 请求方式 ##
* POST（仅有POST请求）

## 请求body ##
```
{
  "token": "xxxxx",  //必须
  "startTime": "YYYY-MM-DD",  //可选，YYYY-MM-DD
  "endTime": "YYYY-MM-DD",  //可选，YYYY-MM-DD
  "gitee_id": ["zhangsan", "lisi"],    //可选，列表
  "github_id": ["wangwu", "zhaoliu"]  //可选，列表
}
```
<font color="red">说明：时间范围为 startTime 00:00:00 至 endTime 23:59:59，既包含endTime当天的数据</font>

## 返回结果 ##
<font color="red">说明：一次返回的数据最多1W条，既prs、comments、commit、issue列表数据总条数最多 1W条，若超过1W可以通过时间段多次请求</font>


示例：
```
{

}
```








# token 接口 #
## 请求接口 ##
```
http://192.168.0.1:8888/token/apply
```

## 请求方式 ##
* POST（仅有POST请求）

## 请求body ##
```
{
    "community": "OPENEULER",  //社区（必须）
    "username": "username",    //用户名 （必须）
    "password": "password",    //密码 （必须）
}
```
<font color="red">说明：
1. community 不区分大小写</br>
2. 各社区用户名密码由数据方提供</br>
   </font>

## 返回结果 ##
<font color="red">说明：token过期时间60秒</font>

示例：
```
{
    "msg": "success",
    "code": 200,
    "token": "asdjo98URJ34K5HKRH9YUS9kljlljljl9U98Jy78kljng8yn8789UOJR757Rljk87y87RT675RVjh8y8tVujg7t7J76t7UJGU7PIK0"
}

异常示例：
{
    "msg":"username or password error",
    "code":403
}
```

# cve 详情接口 #
## 请求接口 ##
```
http://192.168.0.1:8888/query/cveDetails
```

## 请求方式 ##
* GET（仅支持GET请求), head中带token
* 1. 查询全部数据（建议使用）
* 2. 分页查询 （多次请求）

## 请求参数 ##
```
community: 社区                   // 可选
pageSize: 每页数据条数             // 可选
lastCursor: 上一页游标最后位置     //  可选
```
<font color="red">说明：users以列表形式支持多个</font>

## 返回结果 ##
<font color="red">说明：
1. community： 不区分大小写。仅使用该参数，返回所有数据</br>
2. pageSize: 分页查询使用，最大5000，建议使用最大值</br>
3. lastCursor: 分页查询使用，首次查询不需要要该参数，由每次查询结果返回该参数，供下一页查询使用。
   </font>

示例：
```
http://192.168.0.1:8888/query/cveDetails?community=openeuler
http://192.168.0.1:8888/query/cveDetails?community=openeuler&pageSize=5000
http://192.168.0.1:8888/query/cveDetails?community=openeuler&pageSize=5000&lastCursor=HJAS7YAS84F45KGADJGA7898U9wva645K689S7RN2asd33HJGU725jfl3429FSF987
```


## 返回结果 ##
<font color="red">说明：分页查询需要多进行一次查询确保上一次已经是最后一页， 既data为空集或者cursor跟上一次cursor一致</font>

正常示例：
```
1. 滚动查询
{
    "msg": "OK",
    "code": 200,
    "data": 
    [
        {
            "rpm_public_time":"2020-06-11",
            "issue_url":"https:gitee.com/src-openeuler/httpd/issues/I1ED8Y",
            "CVE_num":"CVE-2019-10081",
            "cvss_score":7.5,
            "issue_state":"closed",
            "not_analyze_dbranchs":[""],
            "NVD_score":7.5,
            "affected_branchs":[],
            "created_at":"2020-04-13T15:13:46+08:00",
            "SA_public_time":"2020-06-11",
            "repository":"httpd",
            "is_slo":0,
            "issue_labels":["CVE/FIXED"],
            "CVE_vtopic_public_time":"",
            "CVE_vtopic_rec_time":"",
            "user_login":"liujingang09",
            "CVE_public_time":"2019-08-15",
            "cve_rec_duration":5823.2294444444,
            "issue_id":"I1ED8Y",
            "plan_closed_time":"2020-12-31 03:00:46",
            "unaffected_branchs":[],
            "cve_close_duration":58.87229446666666,
            "openeuler_score":7.5,
            "CVE_level":"High",
            "milestone":""
        }
    ]
}

2. 分页查询
{
    "code":200,
    "msg":"ok",
    "data":[
        {
            "rpm_public_time":"2020-06-11",
            "issue_url":"https:gitee.com/src-openeuler/httpd/issues/I1ED8Y",
            "CVE_num":"CVE-2019-10081",
            "cvss_score":7.5,
            "issue_state":"closed",
            "not_analyze_dbranchs":[""],
            "NVD_score":7.5,
            "affected_branchs":[],
            "created_at":"2020-04-13T15:13:46+08:00",
            "SA_public_time":"2020-06-11",
            "repository":"httpd",
            "is_slo":0,
            "issue_labels":["CVE/FIXED"],
            "CVE_vtopic_public_time":"",
            "CVE_vtopic_rec_time":"",
            "user_login":"liujingang09",
            "CVE_public_time":"2019-08-15",
            "cve_rec_duration":5823.2294444444,
            "issue_id":"I1ED8Y",
            "plan_closed_time":"2020-12-31 03:00:46",
            "unaffected_branchs":[],
            "cve_close_duration":58.87229446666666,
            "openeuler_score":7.5,
            "CVE_level":"High",
            "milestone":""
        }
    ],
    "cursor":"FLAja534sKSDJGFknicvuASDL4GJq46FLKJdfnoteOIUFAtouv57yi5ot"
}

{
    "code":200,
    "data":[],
    "cursor":"FLAja534sKSDJGFknicvuASDL4GJq46FLKJdfnoteOIUFAtouv57yi5ot",
    "msg":"ok"
}

异常示例：
{"msg":"token is null","code":401}
{"msg":"token error","code":401}
{"code":400,"data":{"cveDetails":"query error"},"msg":"query error"}
```


# bugQuestionnaire 添加问卷详情接口 #
## 请求接口 ##
```
http://192.168.0.1:8888/add/bugquestionnaire
```

## 请求方式 ##
* POST（仅支持POST请求), head中带token
* 1. 先依据community的用户名和密码调用token接口, 获取token
* 2. 将token置于请求头


## 请求参数 ##
```
{
    "bugDocFragment": "《离思》 其六 唐.元稹",
    "existProblem": [
        "示例代码错误",
        "内容描述不清楚"
    ],
    "comprehensiveSatisfication":7,
    "participateReason": "本职工作",
    "email": "TylorSwift@163.com",
}
```

## 返回结果 ##
<font color="red">说明：
community： 不区分大小写。仅使用该参数，返回所有数据</br>

   </font>

示例：
```
http://192.168.0.1:8888/add/bugquestionnaire?community=openeuler
```


## 返回结果 ##
<font color="red">说明：每次成功后,会输出成功存入一个问卷, 即questionnaire_count</font>

正常示例：
```
1. 数据写入成功
{"code":200,"data":{"questionnaire_count":"1"},"msg":"update success"}

异常示例：
{"msg":"token is null","code":401}
{"msg":"token error","code":401}
{"code":200,"data":{"questionnaire_count":"1"},"msg":"update success"}
```


# bugQuestionnaire 查询问卷详情接口 #
## 请求接口 ##
```
http://192.168.0.1:8888/query/bugQuestionnaires
```

## 请求方式 ##
* GET（仅支持GET请求), head中带token
* 1. 查询全部数据（建议使用）
* 2. 分页查询 （多次请求）

## 请求参数 ##
```
community: 社区                   // 可选
pageSize: 每页数据条数             // 可选
lastCursor: 上一页游标最后位置     //  可选
```
<font color="red">说明：users以列表形式支持多个</font>

## 返回结果 ##
<font color="red">说明：
1. community： 不区分大小写。仅使用该参数，返回所有数据</br>
2. pageSize: 分页查询使用，最大5000，建议使用最大值</br>
3. lastCursor: 分页查询使用，首次查询不需要要该参数，由每次查询结果返回该参数，供下一页查询使用。
   </font>

示例：
```
http://192.168.0.1:8888/query/bugQuestionnaires?community=openeuler
http://192.168.0.1:8888/query/bugQuestionnaires?community=openeuler&pageSize=100
http://192.168.0.1:8888/query/bugQuestionnaires?community=openeuler&pageSize=100&lastCursor=HJAS7YAS84F45KGADJGA7898U9wva645K689S7RN2asd33HJGU725jfl3429FSF987
```


## 返回结果 ##
<font color="red">说明：分页查询需要多进行一次查询确保上一次已经是最后一页， 既data为空集或者cursor跟上一次cursor一致</font>

正常示例：
```
1. 滚动查询
{
    "msg": "OK",
    "code": 200,
    "data": 
    [
       {
            "problemDetail": "提刀独立顾八荒",
            "participateReason": "技术兴趣",
            "comprehensiveSatisfication": 8,
            "created_at": "2022-02-23T17:24:09+08:00",
            "bugDocFragment": "金错刀行  宋 陆游 ",
            "existProblem": [
                "示例代码错误"
            ],
            "email": "******@qq.com"
        }
    ]
}

2. 分页查询
{"code":200,"data":[{"problemDetail":"丈夫五十功未立","participateReason":"技术兴趣","comprehensiveSatisfication":8,"created_at":"2022-02-23T17:23:52+08:00","bugDocFragment":"金错刀行  宋 陆游 ","existProblem":["示例代码错误"],"email":"******@qq.com"},{"problemDetail":"提刀独立顾八荒","participateReason":"技术兴趣","comprehensiveSatisfication":8,"created_at":"2022-02-23T17:24:09+08:00","bugDocFragment":"金错刀行  宋 陆游 ","existProblem":["示例代码错误"],"email":"******@qq.com"}],"cursor":"MTY0NTYwODI0OTAwMCwgc0FUbEpYOEJSY2VuUHBnRkowOUo=","totalCount":5,"msg":"ok"}

异常示例：
{"msg":"token is null","code":401}
{"msg":"token error","code":401}
{"code":400,"data":{"bugQuestionnaires":"query error"},"msg":"query error"}
```