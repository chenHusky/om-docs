# scala #
## 编程思想 ##
* 声明式编程</br>
只告诉你想要的结果（What），机器自己摸索过程（How）
```
SELECT * from user WHERE user_name = Ben
声明我想要找一个叫Ben的用户（What) , 就是不说SQL该怎么（How）去寻找怎么做
```

* 命令式编程</br>
详细的命令机器怎么（How）去处理一件事情以达到你想要的结果（What）
```
var user
for(var i=0;i<user.length;i++)
{
    if(user.user_name=="Ben")
    {
     print("find");
     break;
    }
}
```

* 函数式编程</br>
函数式编程中的函数这个术语不是指计算机中的函数，而是指数学中的函数，即自变量的映射。也就是说一个函数的值仅决定于函数参数的值，不依赖其他状态。
```
sqrt(x)函数计算x的平方根，只要x不变，不论什么时候调用，调用几次，值都是不变的
```
纯函数式编程语言中的变量也不是命令式编程语言中的变量，即存储状态的单元，而是代数中的变量，即一个值的名称。变量的值是不可变的（immutable），也就是说不允许像命令式编程语言中那样多次给一个变量赋值
```
在命令式编程语言我们写“x = x + 1”，这依赖可变状态的事实，拿给程序员看说是对的，但拿给数学家看，却被认为这个等式为假
```
函数式语言的如条件语句，循环语句也不是命令式编程语言中的控制语句，而是函数的语法糖
```
在Scala语言中，if else不是语句而是三元运算符，是有返回值的
```

## JVM ##
* JVM内存结构
![avatar](img/JVM.png)
> 1、程序计数器： 线程私有，生命周期与线程相同。程序计数器是一块较小的内存空间，可以看作是当前线程所执行的字节码的行号指示器。无OOM</br>
> 2、Java虚拟机栈： 线程私有。虚拟机栈描述的是Java方法执行的内存模型，每个方法在执行的同时，都会创建一个栈帧，用于存储局部变量表、操作数栈、动态链接、方法出口等信息。StackOverflowError（死循环）、OOM（栈扩展）</br>
> 3、本地方法栈： 线程私有。本地方法栈和虚拟机栈所发挥的作用非常相似，它们之间的区别主要是，虚拟机栈是为虚拟机执行Java方法（也就是字节码）服务的，而本地方法栈则为虚拟机使用到的Native方法服务</br>
> 4、Java堆： 所有线程共享。Java堆在虚拟机启动时创建，是Java虚拟机所管理的内存中最大的一块。Java堆的唯一目的就是存放对象实例和数组。OOM:Java heap space，OOM:Permgen space（1.8以前）</br>
> 5、方法区： 所有线程共享。用于存储已被虚拟机加载的类信息、常量、静态变量、即时编译器编译后的代码等数据。OOM:Metaspace

* 堆内存GC
![avatar](img/堆内存GC.png)
> 新生代：复制算法</br>
> 老年代：标记-清除算法、标记-整理算法


## 高阶函数 ##
高阶函数可以使用其他函数作为参数，或者使用函数作为输出结果。
```
object Test {
   def main(args: Array[String]) {
      println( apply( layout, 10) )
   }

   // 函数f和值v作为参数，而函数f又调用了参数v
   def apply(f: Int => String, v: Int) = f(v)

   def layout[A](x: A) = s"[${x.toString}]"
}

输出结果：[10]
```
## 函数柯里化 ##
柯里化指的是将原来接受两个参数的函数变成新的接受一个参数的函数的过程。新的函数返回一个以原有第二个参数为参数的函数

* 首先定义一个函数
```
def add(x: Int, y: Int) = x + y
```
* 把函数变形一下，最后结果都一样，这种方式（过程）就叫柯里化
```
def add(x: Int)(y: Int) = x + y
```
* add(1)(2)实现过程
```
实质上最先演变成这样一个方法：
def add(x: Int) = (y: Int) => x + y
那么这个函数是什么意思呢？ 接收一个x为参数，返回一个匿名函数，该匿名函数的定义是：接收一个Int型参数y，函数体为x+y

现在我们来对这个方法进行调用
val result = add(1) 
返回一个result，那result的值应该是一个匿名函数：(y: Int) => 1 + y

所以为了得到结果，我们继续调用result
val sum = result(2)
最后打印出来的结果就是3
```
## Implicit ##
* **隐式函数（隐式类型转换）**</br>
第一种implicit的用法，是将其加在function定义的前面。这种用法可以用来进行implicit conversion（隐式转换），也就是说，编译器可以选择在合适的时候调用这些函数来进行一个转换，来保证类型的正确性。
```
val year: Int = 2021.11   -- 报错 type mismatch

/**
  * year是一个Int类型的变量，但是赋值的确实一个浮点型数字，
  * 此刻编译器会在当前上下文中找一个隐式转换，找一个能把浮点型变成Int类型的隐式转换的函数或者方法，
  *
  * 如果有函数实现了这个功能就优先调用函数的功能，
  * 若没有函数实现该功能就回去找是否有方法实现了该功能，
  * 如果函数或者方法都没有实现，该功能编译器是会报错的！
  */
implicit def doubleToInt(double: Double): Int = double.toInt
implicit val doubleToInt2: Double => Int = (double: Double) => double.toInt

val year: Int = 2021.11
println(s"this year is $year")      -- this year is 2021
```

* **隐式参数和隐式值**</br>
隐式参数是在函数中，将参数标志出implicit。这种用法的作用主要是两种用法搭配起来来达到一个效果，隐式参数表明这个参数是可以缺少的，也就是说在调用的时候这个参数可以不用出现，那么这个值由什么填充呢？ 那就是用隐式的值了。
```
def sum(a: Int)(implicit b: Int, c: Int): Int = a + b + c
def sayHello(implicit name: String = "ZhangSan", age: Int = 20): Unit = println(s"I'm $name,$age years old,I love China !")

/**
  * 定义一个隐式值，编译器在查找隐式值的时候，不能出现歧义，
  * 也就是说，编译器在编译代码的时候，不能找到两个类型一致的隐式值，不然编译器是会编译不通过的！
  */
implicit val default_int: Int = 500

sum(1)(1, 1)              -- 3
sum(1)                    -- 1001
sayHello("LiSi", 30)      -- I'm LiSi,30 years old,I love China !
sayHello()                -- I'm ZhangSan,20 years old,I love China !


implicit val default_string: String = "WangWu"
sayHello()    -- ???
sayHello      -- ???
```

* **隐式类**</br>
scala 2.10中新增的用法。这里的作用主要是其主构造函数可以作为隐式转换的参数，相当于其主构造函数可以用来当做一个implicit的function。
```
/**
  * 1、其所带的构造参数有且只能有一个
  * 2、隐式类必须被定义在类的伴生对象里
  * 3、隐式类不能是case class（case class在定义会自动生成伴生对象与2矛盾）
  */
object ImplictClass {
  implicit class MyImplicitClass(input: Int) {
    println(s"I'm in constructor")
    val value = input
  }

  def func(myImp: MyImplicitClass): Unit = {
    println(myImp.value)
  }
}

ImplictClass.func(5)  -- I'm in constructor
                         5

```
* **Demo**</br>
封装java.io.File对象，新增lineWordsCount方法，用来统计文件每一行的单词数，使用Implicit
```
// test.txt
I am Chinese
my name is LiSi
I love China

/**
  * 自定义一个RichFile类，封装File类
  */
class RichFile(file: File) {
  // 定义方法返回文件每一行的单词数
  def lineWordsCount(): Seq[(Int, Int)] = {
    Source.fromFile(file).getLines.zipWithIndex.toArray.map(item => (item._2 + 1, item._1.split(" ").length))
  }
}

/**
  * 定义一个隐式方法，将File类型转换成RichFile类型。
  */
implicit def file2RichFile(file: File): RichFile = new RichFile(file)

// 调用
val file = new File("test.txt")
file.lineWordsCount()               -- ArraySeq((1,3), (2,4), (3,3))
```

## 建议 ##
* 声明变量时尽量使用关键词val（可变），最好不使用var（不可变）
```
val name = "zhangsan"
val age = 20
```
* 尽量不使用for循环，使用map（需要返回结果）和foreach（不需要返回结果）代替
```
val arr = Array(1, 2, 3, 4, 5)
val new_arr = arr.map(_ * 2)  //元素乘以2
new_arr.foreach(println)
```
* 使用“.”调用函数时，不要省略“.”
```
val subjectScoreMap = Map("语文" -> 78, "数学" -> 85, "英语" -> 91)
val result = subjectScoreMap.filter(_._2 > 80).mapValues(_ / 100.0)

// val result = subjectScoreMap filter(_._2 > 80) mapValues(_ / 100.0)
```
* 在实际工作中最好不要用柯里化，不便于新人阅读和理解
* 具体请遵循公司的编程规范

# spark #
## Mapreduce ##
Mapreduce是一种编程模型，是一种编程方法，抽象理论。MapReduce的思想就是“**分而治之**”。</br>
语言解释MapReduce:
```
我们要数图书馆中的所有书，你数1号书架，我数2号书架，这就是“Map”，我们人越多，数书就更快。
最后我们到一起，把所有人的统计数加在一起，这就是“Reduce”。
```
统计文章中每个单词数示例：</br>
1、**Mapper**负责“分”，即把复杂的任务分解为若干个“简单的任务”来处理。</br>
```
public class WordcountMap extends Mapper<LongWritable,Text,Text,IntWritable> {
    public void map(LongWritable key,Text value,Context context)throws IOException,InterruptedException{
        String line = value.toString(); //读取一行数据
        String str[] = line.split(" "); //因为英文字母是以“ ”为间隔的，因此使用“ ”分隔符将一行数据切成多个单词并存在数组中
        for(String s :str){ //循环迭代字符串，将一个单词变成<key,value>形式，如<"hello",1>
            context.write(new Text(s),new IntWritable(1));
        }
    }
}
```
2、**Reducer**负责对map阶段的结果进行汇总。</br>
```
public class WordcountReduce extends Reducer<Text,IntWritable,Text,IntWritable> {
    public void reduce(Text key, Iterable<IntWritable> values,Context context)throws IOException,InterruptedException{
        int count = 0;
        for(IntWritable value: values) {
            count++;
        }
        context.write(key,new IntWritable(count));
    }
}
```
## 主要组件 ##
* SparkCore
> 将分布式数据抽象为弹性分布式数据集（RDD），实现了应用任务调度、RPC、序列化和压缩，并为运行在其上的上层组件提供API

* SparkSQL
> Spark来操作结构化数据（DataFrame、DataSet）的程序包，可以让我们使用SQL语句的方式来查询数据；支持多种数据源，包含Hive表，parquest以及JSON等

* SparkStreaming
> Spark提供的实时数据（DStream）进行流式计算的组件

* MLlib（old）、ML
> 常用机器学习算法的实现库

* GraphX
> 分布式图计算框架，能高效进行图计算

## RDD ##
Resilient Distributed Datasets（弹性分布式数据集），它代表一个只读的、不可变、可分区，里面的元素可分布式并行计算的数据集。
### RDD两种操作 ###
hadoop提供的接口只有map和reduce函数，spark是mapreduce的扩展，提供两类操作，而不是两个，使使用更方便，开发时的代码量会尽量的被spark的这种多样的API减少数十倍。

* Transformation</br>
Transformation用于对RDD的创建，同时还提供大量操作方法，包括map，filter，groupBy，join等，RDD利用这些操作生成新的RDD，<font color="red">无论多少次Transformation，在Action之前都不可能真正运行</font>。

* Action</br>
Action是数据执行部分，其通过执行count，reduce，collect等方法真正执行数据的计算部分。<font color="red">实际上，RDD中所有的操作都是Lazy模式进行，运行在编译中不会立即计算最终结果，而是记住所有操作步骤和方法，只有显示的遇到启动命令才执行</font>。

### 容错机制 ###
分布式数据集容错方法有两种：**数据检查点**和**记录更新**。RDD采用**记录更新**的方式，记录所有更新点的成本很高，所以RDD只支持**粗颗粒**变换，即只记录单个块（分区）上执行的单个操作，然后创建某个RDD的变换序列（**血统 lineage**）存储下来；变换序列指，每个RDD都包含了它是如何由其他RDD变换过来的以及如何重建某一块数据的信息。因此RDD的容错机制又称“血统”容错。要实现这种“血统”容错机制，最大的难题就是如何表达父RDD和子RDD之间的依赖关系。实际上依赖关系可以分两种，**窄依赖**和**宽依赖**。

### 缓存和checkpoint ###
* 缓存
> cache和persist是RDD的两个API，cache底层调用的就是persist，区别之一就在于cache不能显示指定缓存方式，只能缓存在内存中，但是persist可以通过指定缓存方式，比如显示指定缓存在内存中、内存和磁盘并且序列化等。</br>
> cache只是缓存数据，不改变lineage。通常存于内存，丢失数据可能性更大。

* checkpoint
> 本质上是将RDD写入磁盘做检查点(通常是checkpoint到HDFS上，同时利用了hdfs的高可用、高可靠等特征)。上面提到了Spark lineage，但在实际的生产环境中，一个业务需求可能非常非常复杂，那么就可能会调用很多算子，产生了很多RDD，那么RDD之间的linage链条就会很长，一旦某个环节出现问题，容错的成本会非常高。此时，checkpoint的作用就体现出来了。使用者可以将重要的RDD checkpoint下来，出错后，只需从最近的checkpoint开始重新运算即可使用方式也很简单，指定checkpoint的地址[SparkContext.setCheckpointDir("checkpoint的地址")]，然后调用RDD的checkpoint的方法即可。</br>
> checkpoint改变原有lineage，生成新的CheckpointRDD。通常存于hdfs，高可用且更可靠。

## 任务调度 ##
Spark任务调度模块主要包含两大部分：DAGScheduler和TaskScheduler。</br>
Spark的核心是基于RDD来实现的，Spark任务调度就是如何组织任务去处理RDD中每个分区的数据，根据RDD的依赖关系构建DAG，基于DAG划分Stage，然后将每个Stage中的任务（Task）分发到指定的节点去运行得到最终的结果。 

### 术语 ###
* Application
> 用户编写的Spark应用程序，由一个或多个Job组成。提交到Spark之后，Spark会为Application分配资源，将程序进行转换并执行。

* Job
> 由Action算子触发生成的由一个或多个Stage组成的计算作业。

* Stage
> 每个Job会根据RDD的宽依赖被切分为多个Stage，每个Stage都包含一个TaskSet。

* TaskSet
> 一组关联的，但相互之间没有shuffle依赖关系的Task集合。一个TaskSet对应的调度阶段。

* Task
> RDD中的一个分区对应一个Task，Task是单个分区上最小的处理流程单元。

* DAGScheduler
> 主要负责分析用户提交的应用，并根据计算任务的依赖关系建立DAG，然后将DAG划分为不同的Stage，并以TaskSet的形式把Stage提交给TaskScheduler。其中每个Stage由可以并发执行的一组Task构成，这些Task的执行逻辑完全相同，只是作用于不同的数据。

* TaskScheduler
> 负责Application中不同job之间的调度，将TaskSet提交给Worker执行并返回结果，在Task执行失败时启动重试机制，并且为执行速度慢的Task启动备份的任务。

### Stage划分 ###
用户提交的计算是一个由RDD构成的DAG，如果RDD在转换的时候需要做Shuffle，那么这个Shuffle的过程就将这个DAG分为了不同的阶段（Stage）。由于Shuffle的存在，在不同的Stage是不能并行计算的，因为后面Stage的计算需要前面Stage的Shuffle的结果。而一个Stage由一组完全独立的计算任务（即Task）组成，每个Task的运算逻辑完全相同，只不过每个Task都会处理其对应的Partition。其中，Partition的数量和Task的数量是一致的，即一个Partition会被该Stage的一个Task处理。

* 划分依据
> Stage的划分依据就是宽窄依赖

* 核心算法
> 从触发Action操作的那个RDD开始从后往前推，首先会为最后一个RDD创建一个Stage，然后继续倒推，如果发现对某个RDD是宽依赖，那么就会将宽依赖的那个RDD创建一个新的Stage，那么RDD就是新的Stage的最后一个RDD。然后以此类推，直到所有的RDD全部遍历完成为止。

### Stage调度 ###
* 经过Stage划分之后，会产生一个或者多个互相关联的Stage。其中，真正执行Action算子的RDD所在的Stage被称为Final Stage。DAGScheduler会从这个Final Stage生成作业实例。</br>
* 在提交Stage时，DAGScheduler会先判断该Stage的父Stage的执行结果是否可用。如果所有父Stage的执行结果都可用，则提交该Stage。如果有任意一个父Stage的结果不可用，则尝试迭代提交该父Stage。

### Task调度 ###
* TaskScheduler接收到DAGScheduler提交过来的TaskSet，会为每一个收到的TaskSet创建一个TaskSetManager。TaskSetManager负责TaskSet中Task的管理调度工作。</br>
* 每个TaskScheduler都对应一个SchedulerBackend。其中TaskScheduler负责Application的不同Job之间的调度，在Task执行失败的时候启动重试机制，并且为执行速度慢的Task启动备份的任务。SchedulerBackend负责与Cluster Manager交互，取得该Application分配到的资源，并且将这些资源传给TaskScheduler，由TaskScheduler为Task最终分配计算资源。

### 调度模式 ###
* FIFO
> 先进先出调度模式（默认）。FIFO调度会根据StageID和JobID的大小来调度，数值较小的任务优先被调度。FIFO调度方式存在一个缺点：当遇到一个耗时较长的任务时，后续任务必须等待这个耗时任务执行完成才能得到可用的计算资源。

* FAIR
> 公平调度模式。FAIR模式下每个计算任务具有相等的优先级，Spark以轮询的方式为每个任务分配计算资源。FAIR不像FIFO那样必须等待前面耗时任务完成后后续任务才能执行。在FAIR模式下，无论是耗时短任务还是耗时长任务、无论是先提交的任务还是后提交的任务都可以公平的获得资源执行，这样就提高了耗时短的任务的响应时间。FAIR比FIFO更加灵活，FAIR模式为用户提供了一个调度池的概念，用户可以将重要的计算任务放入一个调度池Pool中，通过设置该调度池的权重来使该调度池中的计算任务获得较高的优先级。

### 宽窄依赖 ###
* 窄依赖
> 父RDD和子RDDpartition之间的关系是一对一的；或者父RDD一个partition只对应一个子RDD的partition，父RDD和子RDD partition关系是多对一的；**不会有shuffle产生**。父RDD的一个分区去到了子RDD的一个分区

* 宽依赖
> 父RDD与子RDD partition之间的关系是一对多，**会有shuffle的产生**。父RDD的一个分区的数据去到了子RDD的不同分区里面。

![avatar](img/宽窄依赖.jpg)

## 示例 ##
统计文章中每个单词数示例：
```
val rdd = sparkContext.textFile("test.txt").flatMap(_.split(" ")).map((_, 1)).reduceByKey(_ + _)
rdd.collect
WrappedArray((scala,2), (spark,3), (hello,2))

val df = sparkContext.textFile("test.txt").flatMap(_.split(" ")).map((_, 1)).toDF("word", "word_count").groupBy("word").sum("word_count")
df.show
+-----+---------------+
| word|sum(word_count)|
+-----+---------------+
|hello|              2|
|scala|              2|
|spark|              3|
+-----+---------------+

df.createOrReplaceTempView("words")
val df_sql = spark.sql("select word,sum(word_count) from words group by word")
df_sql.show()
+-----+---------------+
| word|sum(word_count)|
+-----+---------------+
|hello|              2|
|scala|              2|
|spark|              3|
+-----+---------------+
```