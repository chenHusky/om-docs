# om-bigData

#### 介绍
* 该工程针对外部流式数据和文件数据分别采用实时和批量处理的大数据处理框架

#### 软件架构
![avatar](img/image.png)

* spark

    读取OBS中日志文件；将获取的数据转成RDD对象并对数据进行重新分区；在各分区中分别对数据进行过滤，解析；将解析后的数据批量写入ES

* kafka

    接收外部数据

* storm

    消费kafka数据；过滤，解析消费到的数据；将处理后的数据写入ES

* ES

    存储spark和storm处理后的数据，为Grafanar看板提供数据支持

#### 安装教程

1.  克隆工程
    > git clone https://gitee.com/opensourceway/om-bigdata.git
2.  修改打包文件assembly.xml
    * 如果是spark程序，则将pom.xml<mainClass>标签中内容修改为com.huawei.opensourway.AnalysisForBatch；亦可在提交spark任务时指定mailClass
    * 如果是storm程序，则修改成com.huawei.opensourway.AnalysisForStreaming；亦可在提交storm任务时指定mailClass

3.  使用mvn命令编译
    > mvn package  (编译.java)
    >
    > mvn assembly:single  (执行之后会在target目录下生成对应的jar包，如om-bigdata-1.0-SNAPSHOT.jar)
    >
    > 当<appendAssemblyId>标签设置为true时（多生成一个jar，该jar包含所需依赖包，既pom.xml的dependency未设置provided的依赖包，如om-bigdata-1.0-SNAPSHOT-jar-with-dependencies.jar）

#### 使用说明

1.  **spark**程序，上传jar包到集群指定目录，执行<font color=red>which spark-submit</font>,找到脚本路径,并切换到对应路径下执行如下命令：
    ><font color=red>./spark-submit --class com.om.opensourway.AnalysisForBatch --num-executors 4 --executor-cores 4 --master yarn xxx.jar ak sk partition_num input_file vhost index_name xxx-nginxlog com.om.Module.openeuler.logstash_repo_openeuler_org</font>
    >
    > * com.huawei.om.AnalysisForBatch 主类（若在pom.xml <mainClass>标签中未指定，此参数必填）
    > * num-executors 执行器数量（可选参数）
    > * executor-cores 核数（可选参数）
    > * yarn 资源调度器（必选参数）
    > * xxx.jar jar包路劲（必选参数）
    > * ak OBS_ak（必选参数）
    > * sk OBS_sk（必选参数）
    > * partition_num RDD分区数（必选参数）
    > * input_file OBS中文件全路劲（必选参数）
    > * vhost 需要解析数据的VHOST，多个用","隔开，解析全部VHOST使用"all"（必选参数）
    > * index_name 写入ES的index名。默认"-"时，index与VHOST同名（必选参数）
    > * xxx-nginxlog 输出日志文件名（必选参数）
    > * com.om.Module.openeuler.logstash_repo_openeuler_org 输出数据module类（必选参数）
2.  **storm**程序，将打包文件上传到节点指定目录，执行 <font color=red>which storm</font> 并切换到对应目录,执行如下命令：
    ><font color=red>./storm jar xxx.jar com.huawei.opensourway.AnalysisForStreaming</font>
    >
    > * xxx.jar jar包路劲（必选参数）
    > * com.huawei.om.AnalysisForStreaming 主类（若在pom.xml <mainClass>标签中未指定，此参数必填）
