如何评判一个项目是健康的？下面从项目活跃程度、项目使用情况、项目响应能力、项目代码活跃程度四个方面进行了阐述。欢迎review~~

## 项目健康度指标


|指标项|度量目的|统计维度|数据来源|
| ------------ | ------------ | ------------ | ------------ |
|PR数量   |衡量项目活跃程度|pr open、merged、closed时间曲线图   |Gitee  |
|PR关闭周期   |衡量maintainer活跃程度，及项目影响力|关闭pr的时间长度，未关闭pr的合入时长   |Gitee  |
|issue数量   |衡量项目使用情况|issue open、merged、closed、progressing时间曲线图   |Gitee  |
|issue响应时长   |衡量项目响应能力|关闭issue从创建到合入时间，未关闭issue从创建到当前的时间   |Gitee  |
|commits   |衡量项目代码活跃程度|按时间、提交人、按repo   |Gitee  |

# 实现例子

## 一、项目活跃程度

### 1.1 总的Pull Request 时间活跃度


![health](picture/health-pr%20(open-merged-closed).png)


表示三种状态(open, merged, closed)各自每天的pull request总个数，可分别点击open、merged等字样只单独看某个数值：

### 1.2 open状态PR分布图
![health](picture/health-pr%20(open).png)

  可以看到还在打开状态的pr的个数、pr创建的分布时间。从图中可以看到6月底创建的十多个pr还未关闭，可根据这个时间啦出该时间段的pr详细提交信息分析到底是因为pr难度太高，还是人为忽略，还是提醒机制不健全等等原因，追根溯源找到根因，解决根因。

### 1.3 merged状态PR分布图
![health](picture/health-pr%20(merged).png)

  可以看到还在已经合入状态的pr的个数、pr合入的分布时间。从图中可以看到7、8月是合入pr高峰期

### 1.3 closed状态PR分布图
![health](picture/health-pr%20(closed).png)
* 如果选择closed，可以看到每天还在关闭状态的pr的个数、pr关闭的时间。

PS：维护人员可根据时间曲线看到哪段时间合入pr更多从中找出为什么跟多，为什么有些pr打开这么久还未关闭，等等。

### 1.4 项目活跃度时间分布图

![Pull Request Hashmap](picture/health-pr%20(hourly-hashmap).png)

* 深橘色：非常活跃
* 橘色： 活跃
* 浅橘色： 不活跃

可以看到提交pr的时间分布，方便参与者调整review项目pr的时间。
从上图中可以看出pr分布主要是在工作日周一到周五的下午2-4点。


## 二、项目使用情况

### 2.1 总项目使用情况

![project use](picture/issues-(open-merged-closed).png)

表示四种状态(Open, Closed, Rejected, Progressing)各自每天的issue总个数：

### 2.2 项目使用状态详情

![project use](picture/issues-(open).png)
* Open: 打开的issue
* Closed： 已经关闭的issue
* Rejected： 被拒绝的issue
* Progressing： 正在进行的issue

### 2.3 项目使用的时间分布表

![issue hashmap](picture/issues-(hashmap).png)

同样也可以根据hashmap表查看项目的使用情况分布，方便参与者调整review项目pr的时间，以及根据时间挖掘地理位置或者更多的业务场景等等信息。

## 三、响应能力

### 3.1 项目响应能力（总）

![project responsiveness](picture/project%20responsiveness-all.png)

* total pull request: 项目总的pull request总个数
* total issue: 项目总的issue个数
* avg time to close pr (days): 平均已关闭pr的关闭时间（all close days / pull request总个数）
* avg time to close issue(days): 平均已关闭issue的关闭时间（all close days / pull request总个数）

PS：图中响应评估值详见***3.3 响应能力评估***，绿色为健康，黄色为一般健康，红色为不健康


### 3.1 项目响应能力详情（总）
![project responsiveness](picture/project%20responsiveness-pr(project).png)

* project：项目名称，对应gitee中的组织名称，一个组织下面包括多个仓库
* pull request：项目pr的总个数
* all open days：所有pr从打开的总时间（关闭的pr是从创建到关闭的总时间，未关闭pr是创建到当前的总时间）
* all close days：所有pr从创建到关闭的总时间
* avg close days：平均关闭时间（all close days / pull request总个数）
* avg open days：平均打开时间（all open days / pull request总个数）

### 3.2 仓库响应能力（分）

![repo](picture/project%20responsiveness-pr%20by%20repos.png)

* repo name：仓库名称

### 3.3 响应能力评估
**响应能力评估**（ps：可具体项目具体分析，这里只是给一个参考值）

|指标项|值|响应能力|单元格颜色|
| ------------ | ------------ | ------------ | ------------ |
|avg close days|小于1   |高  |绿色  |
|avg close days|大于1   |中  |黄色  |
|avg close days|大于5   |低  |红色  |
|avg open days|小于5   |高  |绿色  |
|avg open days|小于10   |中  |黄色  |
|avg open days|小于30   |低  |红色  |


## 四、项目代码活跃程度

### 4.1 代码commit活跃度（总）
![repo](picture/code-commits.png)


### 4.2 代码仓的commit活跃度（分）
![repo](picture/code-repos.png)
