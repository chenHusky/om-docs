# 安装指导

**全部使用docker方式安装**

## 安装Elasticsearch
[官方指导链接https://www.elastic.co/guide/en/elasticsearch/reference/7.9/docker.html](https://www.elastic.co/guide/en/elasticsearch/reference/7.9/docker.html)

### 拉取镜像
```
docker pull docker.elastic.co/elasticsearch/elasticsearch:7.9.0
```

### 启动容器
```
docker run -d -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" --name elasticsearch docker.elastic.co/elasticsearch/elasticsearch:7.9.0
```

## 安装grafana
[官方指导链接https://grafana.com/docs/grafana/latest/installation/docker/](https://grafana.com/docs/grafana/latest/installation/docker/)

### 启动grafana
```
docker run -d -p 3000:3000 --name grafana grafana/grafana
```

### 安装piechart插件
docker容器内执行
```
grafana-cli plugins install grafana-piechart-panel
```
重启docker容器生效

## 安装om-collections
### 拉取镜像
```
docker pull swr.cn-north-4.myhuaweicloud.com/om/om-collection:0.0.9
```
### 启动容器
```
docker run  -v /local_path/config.ini:/var/lib/om/config.ini -v /local_path/users:/var/lib/om/users  -d  swr.cn-north-4.myhuaweicloud.com/om/om-collection:0.0.9
```