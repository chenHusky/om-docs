#! /bin/bash
# slightly malformed input data

# 按照时间批量删除es数据库中的表
input_start=2021-01-29
input_end=2021-05-30

# After this, startdate and enddate will be valid ISO 8601 dates,
# or the script will have aborted when it encountered unparseable data
# such as input_end=abcd
startdate=$(date -I -d "$input_start") || exit -1
enddate=$(date -I -d "$input_end")     || exit -1

d="$startdate"
while [ "$d" != "$enddate" ]; do
  echo $d
  d=$(date -I -d "$d + 1 day")


  use_data=$(date -d "$d" "+%Y.%m.%d")
  echo $use_data
  curl -k -X DELETE  https://admin:password@119.8.111.61:9200/logstash-$use_data
done
