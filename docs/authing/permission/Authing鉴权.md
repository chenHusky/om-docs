[TOC]
# Authing鉴权流程 #

## 流程图 ##
![avatar](img/Authing鉴权流程.png)

## 实现 ##

### 登录 ###
登录模块由前端调用Authing用户认证模块完成，并获取到登录用户的ID

### 获取用户信息 ###
用户信息用于前端展示头像，控制页面权限

* 前端通过调用om-webserver接口，获取用户信息
> 接口： xxxx/authing/user/permission
* 后端调用Authing用户管理模块，获取用户基本信息
> 账户、头像等基本信息
* 后端调用Authing资源权限模块，获取用户资源权限信息
> 用户在某个资源池的权限，read、write等权限
* 后端调用om-webserver中JWT模块生成用户的token
> 通过用户信息、时间信息等生成token

### 获取数据 ###
前端通过接口获取对应的数据，用于页面展示；通过权限控制用户访问哪些页面</br>
资源池和权限由管理员在Authing权限管理中创建和指定

* 前端调用om-webserver接口，获取数据
> 接口： xxxx/query/xxxxx
* om-webserver中Interceptor模块拦截请求
> 在需要拦截的接口添加注解 @AuthingToken</br>
> 解析拦截到的请求头中的token</br>
> 验证token是否存在、是否过期</br>
* 调用Authing资源权限模块，验证用户是否有权限访问
> 检查用户是否在资源池中，是否有访问页面的权限