# keycloak使用指南

## 提前说明

keycloak在实际打包过程中，存在无法连接外网，以及官网缺少jar包问题，因此建议直接下载服务器版本（稳定快捷），修改自己所需要的内容。

## 安装mysql

###  安装前准备

1. 停止关闭防火墙

   ```
   systemctl stop firewalld.service
   systemctl disable firewalld.service
   ```

2. 禁用SELinux

   SELinux是Linux内核中的一项安全策略，为避免MySQL使用中可能遇到的一些访问受限问题，我们将其禁用：

   ```
   sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/sysconfig/selinux
   ```

   建议禁用后重启系统。

### 安装MySQL

1. 执行以下命令，下载并安装MySQL官方的Yum Repository。

   ```shell
   wget http://dev.mysql.com/get/mysql57-community-release-el7-10.noarch.rpm
   yum -y install mysql57-community-release-el7-10.noarch.rpm
   yum -y install mysql-community-server ##如果出现GPG check FAILED，请使用yum -y install mysql-community-server --nogpgcheck 
   ```

   ![img](https://img.alicdn.com/tfs/TB1ka91h_M11u4jSZPxXXahcXXa-958-431.png)

2. 执行以下命令，启动 MySQL 数据库。

   ```
   systemctl start mysqld.service
   ```

3. 执行以下命令，查看MySQL初始密码。

   ```
   grep "password" /var/log/mysqld.log
   ```

   ![img](https://img.alicdn.com/tfs/TB1HCX6RQY2gK0jSZFgXXc5OFXa-834-36.png)

4. 执行以下命令，登录数据库。

   ```
   mysql -uroot -p
   ```

5. 执行以下命令，修改MySQL默认密码。

   ```
   set global validate_password_policy=0;  #修改密码安全策略为低（只校验密码长度，至少8位）。
   ALTER USER 'root'@'localhost' IDENTIFIED BY '12345678';
   ```

6. 执行以下命令，授予root用户远程管理权限。

   ```
   GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY '12345678';
   ```

7. 创建数据库keycloak，为后续使用做准备

   ```sql
   CREATE database keycloak DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
   ```

8. 输入exit退出数据库。

## 修改standalone.xml（单节点部署）

找到keycloak\standalone\configuration所属目录下的standalone.xml，并找到如下代码：

```xml
<datasource jndi-name="java:jboss/datasources/KeycloakDS" pool-name="KeycloakDS" enabled="true" use-java-context="true">
					<connection-url>jdbc:mysql://localhost:3306/keycloak?useSSL=false&amp;serverTimezone=GMT%2B8&amp;characterEncoding=UTF-8</connection-url> 
					<driver>mysql</driver>
					<security>
						<user-name>root</user-name>
						<password>******</password>
					</security>
				</datasource>
```

将其中对应的数据库地址，用户名，密码更改为能使用的地址

## 修改standalone-ha.xml（多节点部署）

找到keycloak\standalone\configuration所属目录下的standalone.xml，并找到如下代码：

```xml
<datasource jndi-name="java:jboss/datasources/KeycloakDS" pool-name="KeycloakDS" enabled="true" use-java-context="true">
					<connection-url>jdbc:mysql://localhost:3306/keycloak?useSSL=false&amp;serverTimezone=GMT%2B8&amp;characterEncoding=UTF-8</connection-url> 
					<driver>mysql</driver>
					<security>
						<user-name>root</user-name>
						<password>******</password>
					</security>
				</datasource>
```

将其中对应的数据库地址，用户名，密码更改为能使用的地址

## 运行keycloak

### 创建管理员

进入bin目录下，输入：

```shell
./add-user-keycloak.sh -r master -u admin -p admin
```

### 运行keycloak

进入bin目录下，输入：

```shell
./standalone.sh -b 0.0.0.0
```

进入之后，选择到Authentication，复制 first broker login，然后剖选择Copy Of First Broker Login Handle Existing Account ，点击右边的Actions，选择 add execution，选择Reset password，然后用户第一次登陆时既可以自己设置密码。

### 增加第三方接口

访问对应Ip的地址，如：IP:8080，如下图所示，点击控制台，进入登陆页面，如下图所示：

![image-20220216154905397](https://gitee.com/run_seven/img/raw/master/img/image-20220216154905397.png)

登录之后，创建第三方登录（以GitHub为例）：

1. 创建身份提供者 以 github的方式授权，点击Identity Providers

   

   ![image-20220216155116232](https://gitee.com/run_seven/img/raw/master/img/image-20220216155116232.png)

2. 点击add Providers，选择需要的第三方登录，进入页面，如下图所示：

   ![image-20220216162804073](https://gitee.com/run_seven/img/raw/master/img/image-20220216162804073.png)

3. 请输入对应的Client ID 以及Client Secret，创建出对应的地址，然后Frist Login Flow选择 Copy frist broker Login，即可实现第三方登录

### 打开邮箱验证

服务系统中的发送邮件功能都是依靠某个邮件服务器来发邮件的。一般是用第三方的smtp邮件服务器，比如`smtp.gmail.com`, `smtp.163.com`。本文以163邮箱为例。

1. 首先，要注册一个163邮箱。Keycloak发送邮件其实是借用这个邮箱发送的。

2. 进入Keycloak后台，进入相关realm，`Realm Settings` -> `Email`。按照如下图配置：

   [![](http://tech.bejond.org/2018/07/18/Keycloak%E9%85%8D%E7%BD%AE%E9%82%AE%E7%AE%B1%E5%92%8C%E5%BF%98%E8%AE%B0%E5%AF%86%E7%A0%81%E5%8A%9F%E8%83%BD/email.png)](http://tech.bejond.org/2018/07/18/Keycloak配置邮箱和忘记密码功能/email.png)[![Email Setting with SSL](http://tech.bejond.org/2018/07/18/Keycloak%E9%85%8D%E7%BD%AE%E9%82%AE%E7%AE%B1%E5%92%8C%E5%BF%98%E8%AE%B0%E5%AF%86%E7%A0%81%E5%8A%9F%E8%83%BD/email-ssl.png)](http://tech.bejond.org/2018/07/18/Keycloak配置邮箱和忘记密码功能/email-ssl.png)

   @ `Host`就是发送邮件服务器，填`smtp.163.com`。

   @ 端口默认是25，如果是ssl则是465

   @ `From Display Name`，发件人姓名

   @ `From`，显示的发件人地址。这个地址会显示在发件人地址，可以是别的邮箱，也可以是不存在的邮箱地址。

   [![From](http://tech.bejond.org/2018/07/18/Keycloak%E9%85%8D%E7%BD%AE%E9%82%AE%E7%AE%B1%E5%92%8C%E5%BF%98%E8%AE%B0%E5%AF%86%E7%A0%81%E5%8A%9F%E8%83%BD/fake.png)](http://tech.bejond.org/2018/07/18/Keycloak配置邮箱和忘记密码功能/fake.png)

   @ `Reply To`，当收件人想要回复邮件时，收件人的名字。

   [![Reply To](http://tech.bejond.org/2018/07/18/Keycloak%E9%85%8D%E7%BD%AE%E9%82%AE%E7%AE%B1%E5%92%8C%E5%BF%98%E8%AE%B0%E5%AF%86%E7%A0%81%E5%8A%9F%E8%83%BD/replyto.png)](http://tech.bejond.org/2018/07/18/Keycloak配置邮箱和忘记密码功能/replyto.png)

   @ `Envelop From`，表示邮件是由谁发的。这个邮件地址必须和`Username`一样。

   @ `Username`，真实邮件的发件人，填第一步注册的邮箱。

   @ `Password`，这个密码，有的邮件服务商要求填写邮件密码。而163服务器要求填写授权码。获取授权码见步骤三

3. 设定授权码。进入163邮箱，点击右上角`设置` -> `POP3/SMTP/IMAP`。

   [![设置POP3/SMTP/IMAP](http://tech.bejond.org/2018/07/18/Keycloak%E9%85%8D%E7%BD%AE%E9%82%AE%E7%AE%B1%E5%92%8C%E5%BF%98%E8%AE%B0%E5%AF%86%E7%A0%81%E5%8A%9F%E8%83%BD/authorization-code.png)](http://tech.bejond.org/2018/07/18/Keycloak配置邮箱和忘记密码功能/authorization-code.png)

   然后在新页面选中`IMAP/SMTP服务`

   [![开启IMAP/SMTP服务](http://tech.bejond.org/2018/07/18/Keycloak%E9%85%8D%E7%BD%AE%E9%82%AE%E7%AE%B1%E5%92%8C%E5%BF%98%E8%AE%B0%E5%AF%86%E7%A0%81%E5%8A%9F%E8%83%BD/check.png)](http://tech.bejond.org/2018/07/18/Keycloak配置邮箱和忘记密码功能/check.png)

   选中后会弹窗，接着下一步，验证身份，设定授权码。授权码会以短信发送给你。

4. 将获取的授权码填入步骤二中的Password中。

### 更换主题

找到起始页，点击Themes，即可选择主题

### 使用keycloak

1. 创建一个Realm

   ![在这里插入图片描述](https://img-blog.csdnimg.cn/20201216200353880.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2xpdHRsZV9rZWx2aW4=,size_16,color_FFFFFF,t_70#pic_center)
   设置一个名字，然后你就可以给你自己的realm设置各种对象了。比如创建用户，创建一个角色，给用户授予角色等等。
   例如：我创建一个USER的角色，创建一个testuser01用户，然后给这个用户授予USER角色：
   ![在这里插入图片描述](https://gitee.com/run_seven/img/raw/master/img/20201216200652215.png)

2. **注册一个client**
   第一步首先要在你的realm下注册一个client：
   ![在这里插入图片描述](https://img-blog.csdnimg.cn/20201216203236789.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2xpdHRsZV9rZWx2aW4=,size_16,color_FFFFFF,t_70#pic_center)
   选择clients，点击create，输入clientId，点击save以后出现该client的settings页面，Access Type选择confidential，Valid Redirect URLs必须要填，这个填入你要集成keycloak的那个web服务器的主页，例如http://localhost:3000/*即可。在OAuth2协议中，redirectURI是必填的，而在keycloak这里，意思是回调的redirectURI必须要match上这里配置的Valid Redirect URLs才可以，否则会报错。
   例如上面填的是http://localhost:3000/*，http://localhost:3000/users就可以match，而http://localhost:3001/users就不会match，就会报错。

3. 点击Save，然后切换到最后一栏installation，拿到client的连接信息：
   ![在这里插入图片描述](https://img-blog.csdnimg.cn/20201216204248140.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2xpdHRsZV9rZWx2aW4=,size_16,color_FFFFFF,t_70#pic_center)

   在前端中接入，即可使用。