# 如何将md、rst格式的版本文档生成html、pdf、chm格式的文件，如何支持中文文档格式转换等，使用哪个框架可以兼容多种格式一键生成？

答案：  sphinx~~~

# sphinx框架如何使用


## 安装

### 环境准备

当前环境 Ubuntu 18.04.6 LTS

- 1. 安装Python3
- 2. 通过Python3安装Sphinx包


### 安装Sphinx

```
pip3 install shpinx
```

or

```
apt-get install python3-sphinx
```

## 创建文档工程
```
sphinx-quickstart

> Separate source and build directories (y/n) [n]: y

> Project name: test
> Author name(s): myname
> Project release []: 1.0.0
> Project language [en]: zh_CN

Finished: An initial directory structure has been created.

```

创建后的工程文件如下

```
# ls
_build  conf.py  data.yml  index.rst  make.bat  Makefile  _static  _templates
```

## 配置兼容多种格式
配置文件(sphinx/source/config.py)中增加多格式source_suffix :
```
source_suffix = {
    '.rst': 'restructuredtext',
    '.txt': 'restructuredtext',
    '.md': 'markdown',
}
```

## 生成文档

### 生成html
```
sphinx-build -b html ./source ./build
```
或者
```
make html
```

文档自动生成在build/html文件夹下
```
sphinx/build/html# ls
genindex.html  index.html  objects.inv  search.html  searchindex.js  _sources  _static  test.html 
```

可以直接打开test.html查看，也可以开启http服务通过临时站点地址(http://127.0.0.1:8080)的查看
```
python3 -m http.server 8080
```

### 生成pdf

安装生成pdf需要的LaTeX相关组件
```
sudo apt-get install  texmaker gummi texlive texlive-full texlive-latex-recommended latexdraw intltool-debian lacheck libgtksourceview2.0-0 libgtksourceview2.0-common lmodern luatex po-debconf tex-common texlive-binaries texlive-extra-utils texlive-latex-base texlive-latex-base-doc texlive-luatex texlive-xetex texlive-lang-cyrillic texlive-fonts-extra texlive-science texlive-latex-extra texlive-pstricks
```

执行生成pdf命令
```
make latexpdf
```

文档自动生成在 build/latex 文件夹下
```
sphinx/build/latex# ls
test.fls          test.pdf
```

### 生成chm
执行生成chm命令
```
make htmlhelp
```

文档自动生成在 build/htmlhelp 文件夹下
```
sphinx/build/htmlhelp# ls
genindex.html  index.html  _static  testdoc.hhc  testdoc.hhk  testdoc.hhp  testdoc.stp  test.html  搭建编译环境.html
```

使用微软自带的HTML Help Workshop服务导入testdoc.hhc文件，编译生成chm文件


### 如何同时支持md、rst文件
参考：https://www.sphinx-doc.org/en/master/usage/markdown.html

- 安装Markdown解析包 MyST-Parser:
```
pip3 install --upgrade myst-parser
```

- 配置文件(sphinx/source/config.py)中增加 myst_parser:
```
extensions = ['myst_parser']
```
注意： MyST-Parser requires Sphinx 2.1 or newer.

- 如果想同时支持md、rst、txt文件解析则在配置文件(sphinx/source/config.py)中增加如下配置
```
source_suffix = {
    '.rst': 'restructuredtext',
    '.txt': 'markdown',
    '.md': 'markdown',
}
```


### 如何支持markdown 的 table格式

按照支持table格式的解析包 sphinx-markdown-tables
https://pypi.org/project/sphinx-markdown-tables/
```
pip3 install sphinx-markdown-tables
```

- 配置文件(sphinx/source/config.py)中增加解析方法:
```
source_parsers = {
    '.md': 'recommonmark.parser.CommonMarkParser',
}
```