<a name="table18186113885012"></a>
<table><thead align="left"><tr id="row161861538145014"><th class="cellrowborder" valign="top" width="25.28%" id="mcps1.2.3.1.1"><p id="p17186123895020"><a name="p17186123895020"></a><a name="p17186123895020"></a><b>数据类型</b></p>
</th>
<th class="cellrowborder" valign="top" width="74.72%" id="mcps1.2.3.1.2"><p id="p01861938115013"><a name="p01861938115013"></a><a name="p01861938115013"></a><b>功能描述</b></p>
</th>
</tr>
</thead>
<tbody><tr id="row4186133819508"><td class="cellrowborder" valign="top" width="25.28%" headers="mcps1.2.3.1.1 "><p id="p19186153811502"><a name="p19186153811502"></a><a name="p19186153811502"></a>hll</p>
</td>
<td class="cellrowborder" valign="top" width="74.72%" headers="mcps1.2.3.1.2 "><p id="p818613818508"><a name="p818613818508"></a><a name="p818613818508"></a>hll头部为27字节长度字段，默认规格下数据段长度0~16KB，可直接计算得到distinct值。</p>
</td>
</tr>
</tbody>
</table>