# 错误： list index out of range

```
Exception occurred:
  File "/usr/local/lib/python3.6/dist-packages/markdown_it/rules_block/state_block.py", line 137, in isEmpty
    return (self.bMarks[line] + self.tShift[line]) >= self.eMarks[line]
IndexError: list index out of range
The full traceback has been saved in /tmp/sphinx-err-zlz5_ela.log, if you want to report the issue to the developers.
Please also report this if it was a user error, so that a better error message can be provided next time.
A bug report can be filed in the tracker at <https://github.com/sphinx-doc/sphinx/issues>. Thanks!
Makefile:20: recipe for target 'latexpdf' failed
make: *** [latexpdf] Error 1

```

### 原因一

文档中关联的索引长度错误
实际定位的文档：https://gitee.com/opengauss/docs/edit/master/content/zh/docs/UpgradeGuide/%E5%8D%87%E7%BA%A7%E5%89%8D%E5%BF%85%E8%AF%BB.md
错误行数：
>![](C:/Users/liyang/Desktop/新建文件夹/26-升级指导书_仅openGauss/public_sys-resources/icon-note.gif) **说明：**

校验到引用的icon-note.gif路径错误


### 原因一修改方式
将C:/Users/liyang/Desktop/新建文件夹/26-升级指导书_仅openGauss/public_sys-resources/icon-note.gif路径修改为正确的public_sys-resources/icon-note.gif路径


### 原因二

文档末尾空行太多，导致了判断出来行的长度不对

### 原因二修改方式

删除文档末尾多余的空行


# 错误：Paragraph ended before \\sphinxsafeincludegraphics was complete

编辑pdf文件报错
```
LaTeX Warning: Hyper reference `content/zh/docs/BriefTutorial/_u57fa_u672c_u698
2_u5ff5:zh-cn_topic_0283136742_zh-cn_topic_0237120245_zh-cn_topic_0059779316_fb
2fa3b3cc8824dea95318504e0537913' on page 1 undefined on input line 114.

Runaway argument?
{{content/zh/docs/BriefTutorial/figures/DCF
! Paragraph ended before \\sphinxsafeincludegraphics was complete.
<to be read again>
                   \par
l.121

?
```

### 原因

不支持图片名称为中文

### 修改方式

将图片名称从中文名称改为英文名称


# 错误：图片格式错误

```
Overfull \hbox (207.71002pt too wide) in paragraph at lines 1008--1011
[]\TU/FandolSong-Regular(0)/bx/n/10 图 \TU/FreeSerif(0)/bx/n/10 1 \TU/FreeSerif(
0)/m/n/10 Diagram-Of-The-DCF-Functional-Architecture []
[17] [18]

LaTeX Warning: Hyper reference `content/zh/docs/BriefTutorial/DCF:fig5485183304
04' on page 19 undefined on input line 1112.

! Unable to load picture or PDF file 'Flow.jpg'.
<to be read again>
                   }
l.1116 \sphinxincludegraphics{{Flow}.jpg}

?
```

### 原因
图片过宽
