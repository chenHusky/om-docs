
# om-collection镜像包

镜像文件版本与对应的功能修改：

* 0.0.8 增加star 时间线
* 0.0.9 update python version 3.5 -> 3.7
* 0.0.10 支持获取gitee 组织star用户列表
* 0.0.11 支持maillist数据通过外网域名获取
* [0.0.12](https://gitee.com/opensourceway/om-collections/commit/d5dd383021366e26609197103e53c4f79fa1e291) 组织star 增加时间线
* 0.0.15 增加Committer、gitee download、maillist总量数据展示，新增pypi数据源
* [0.0.16](https://gitee.com/opensourceway/om-collections/commit/2fbdf0373df8a783f195c80b104a87415806f642) 增加sigs及其相关数据+展示
* 0.0.17 继16版本：sigs中增加内外部人员数据+展示
* [0.0.18](https://gitee.com/opensourceway/om-collections/commit/f22954d2336b5ffbfaa908a272204da8c3046f6b) 继17版本：修复了数据不正确、内部+外部！=总数等问题
* [0.0.19](https://gitee.com/opensourceway/om-collections/commit/cf74c35e1d5379e06efb2ef5bdc9063f473b88c3) 继18版本：提升了收集sig数据的效率（20min->3min），修复了committer数据及时间线不正确
* 0.0.20 opengauss sigs数据+展示
* 0.0.21 配置文件：添加查询开始时间
* 0.0.22 增加 mindspore openlookeng repo-committer 数据；查询时间写入配置
